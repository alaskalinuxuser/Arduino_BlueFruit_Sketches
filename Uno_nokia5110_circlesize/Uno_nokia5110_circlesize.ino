/*
This is an example on how to use the display known as Nokia 5110 SPI with PCD8544 driver using the U8GLIB library.

Nokia 5110 SPI display pins for Arduino Uno/Nano:
 * RST =    8
 * CE =     10
 * DC =     9
 * DIN =    11
 * CLK =    13
 * VCC =    5V
 * BL  =    3.3V
 * GND =    GROUND

We use this library: https://github.com/olikraus/U8glib_Arduino

User Reference Manual: https://github.com/olikraus/u8glib/wiki/userreference

List of fonts: https://github.com/olikraus/u8glib/wiki/fontsize

Sketch Originally made by: InterlinkKnight
This sketch was featured on his video tutorial for this display: https://www.youtube.com/watch?v=1ZvY_lb6BoU
Last modification: 11/22/2017

Sketch modified by alaskalinuxuser
Last modification: 04/02/2019
*/


 
#include "U8glib.h"  // Include U8glib library


// Create display and set pins:
// U8GLIB_PCD8544 u8g(3, 4, 13, 5, 12);   // (CLK=13, DIN=11, CE=10, DC=9, RST=8)
// Alternate wiring:
U8GLIB_PCD8544 u8g(3, 4, 13, 5, 12);   // (CLK=3, DIN=4, CE=13, DC=5, RST=12)

int Variable1;  // Create a variable to have something dynamic to show on the display
int fadeAmount = 1;    // how many points to change by

void setup(void)  // Start of setup
{

  // Select a font:
  u8g.setFont(u8g_font_profont12);

  // Change display orientation:
  //u8g.setRot90();
  u8g.setRot180();
  //u8g.setRot270();

}  // End of setup

void loop(void)  // Start of loop
{
    // wait for 25 milliseconds to see the dimming effect
  delay(25);

  // change the brightness for next time through the loop:
  Variable1 = Variable1 + fadeAmount;

  // reverse the direction of the fading at the ends of the fade:
  if (Variable1 <= 0 || Variable1 >= 45) {
    fadeAmount = -fadeAmount;
  }

  u8g.firstPage(); // Beginning of the picture loop
  do  // Include all you want to show on the display:
  {
    u8g.drawCircle(41, 21, Variable1, U8G_DRAW_ALL);  // Draw a circle (x,y,radius,option)

    u8g.drawRFrame(59, 10, 25, 13, 3);  // Draw a rounded square (x,y,width,height,radius)

    // Variable with left text alignment:
    u8g.setPrintPos(63, 20);  // (x,y)
    u8g.print(Variable1);  // Value to print


  } while(u8g.nextPage());  // End of the picture loop
  
}  // End of loop
