/*  Copyright 2019 by AlaskaLinuxUser (https://thealaskalinuxuser.wordpress.com)
*
*   Licensed under the Apache License, Version 2.0 (the "License");
*   you may not use this file except in compliance with the License.
*   You may obtain a copy of the License at
*
*       http://www.apache.org/licenses/LICENSE-2.0
*
*   Unless required by applicable law or agreed to in writing, software
*   distributed under the License is distributed on an "AS IS" BASIS,
*   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*   See the License for the specific language governing permissions and
*   limitations under the License.
*/

/*
 * A special note of credit goes to Mark Bramwell, who originally wrote the 
 * LDC keyshield test program, back in July of 2010, from which I learned how to utilize
 * the buttons and program this shield into a game.
 */

#include <LiquidCrystal.h>

LiquidCrystal lcd(8, 9, 4, 5, 6, 7);           // select the pins used on the LCD panel

// define some values used by the panel and buttons
int lcd_key     = 0;
int adc_key_in  = 0;

// Game variables:
int gameLineNum = 0; // A way to switch between display lines.
int gameState   = 0; // The state of game play.
int gameBullets = 9; // Number of bullets remaining.
int gameSpeed = 1; // The speed of the game.
int gameScore = 0; // Number of asteroids destroyed or past.
int gamePosition = 0; // Your space ships location, 0 for up, 1 for down.
int gameAsteroidOne = 14; // First asteroid starting space.
int gameAsteroidTwo = 25; // Second asteroid starting space.
int gameAsteroidThree = 17; // Third asteroid starting space.
int gameAsteroidFour = 23; // Fourth Asteroid starting space.
int gameDelay = 0; // A time delay factor.
bool gameShoot = true; // true, you can shoot now.
bool gamePaused = false; // the game is not paused currently.
int gameTime = 0; // A variable to smooth out the pause button.

/*
 * Game states:
 * 0 = Asteroid start screen
 * 1 = Credits
 * 2 = How to info
 * 3 = Play mode
 * 4 = Pause mode
 * 5 = win screen
 * 6 = lose screen
 * 
 * Game display: 16 x 2.
 * 
 * B B B W S A A A A A A A A A A A
 * P P P W S A A A A A A A A A A A
 * 
 * B = Bullets left
 * P = Past Asteroids
 * W = Wall of Game
 * S = Space Ship
 * A = Asteroid Field
 */

#define btnRIGHT  0
#define btnUP     1
#define btnDOWN   2
#define btnLEFT   3
#define btnSELECT 4
#define btnNONE   5

int read_LCD_buttons(){               // read the buttons
    adc_key_in = analogRead(0);       // read the value from the sensor 

    // my buttons when read are centered at these valies: 0, 144, 329, 504, 741
    // we add approx 50 to those values and check to see if we are close
    // We make this the 1st option for speed reasons since it will be the most likely result

    if (adc_key_in > 1000) return btnNONE; 

    // For V1.1 us this threshold
    if (adc_key_in < 50)   return btnRIGHT;  
    if (adc_key_in < 250)  return btnUP; 
    if (adc_key_in < 450)  return btnDOWN; 
    if (adc_key_in < 650)  return btnLEFT; 
    if (adc_key_in < 850)  return btnSELECT;  

   // For V1.0 comment the other threshold and use the one below:
   /*
     if (adc_key_in < 50)   return btnRIGHT;  
     if (adc_key_in < 195)  return btnUP; 
     if (adc_key_in < 380)  return btnDOWN; 
     if (adc_key_in < 555)  return btnLEFT; 
     if (adc_key_in < 790)  return btnSELECT;   
   */

    return btnNONE;                // when all others fail, return this.
}

int set_gameSpeed (int gameScore){
      int a = 1;
        if (gameScore < 10){
          a = 1;
        } else if (gameScore < 25) {
          a = 2;
        }else if (gameScore < 50) {
          a = 3;
        } else if (gameScore < 75) {
          a = 4;
        }
      return a;
}

int get_random (int addOn) {
  srand(millis()+addOn); // Use time since turned on and add on number as seed for random number.
  int r = rand() % 10; // Returns a random integer between 0 and 10.
  r += 10; // Add 10 to keep it off of the display screen.
  return r;
}

void setup(){
   lcd.begin(16, 2);               // start the library
   lcd.setCursor(0,0);             // set the LCD cursor   position 
   lcd.print("Asteroids Game!");  // print a simple message on the LCD
}
 
void loop(){

  if (gameState == 0) { // start screen state.
    // The initial start screen. Let's set all game variables for a new game.
      // Game variables:
      lcd.setCursor(0,0); // set the LCD cursor   position 
      lcd.print("Asteroids Game!");  // print a simple message on the LCD
       gameState   = 0; // The state of game play.
       gameBullets = 9; // Number of bullets remaining.
       gameSpeed = 1; // The speed of the game.
       gameScore = 0; // Number of asteroids destroyed or past.
       gamePosition = 0; // Your space ships location, 0 for up, 1 for down.
       gameAsteroidOne = 14; // First asteroid starting space.
       gameAsteroidTwo = 25; // Second asteroid starting space.
       gameAsteroidThree = 17; // Third asteroid starting space.
       gameAsteroidFour = 23; // Fourth Asteroid starting space.
       gameShoot = true; // true, you can shoot now.
       gamePaused = false; // the game is not paused currently.
       gameTime = 0; // A variable to smooth out pause function.
  
     lcd.setCursor(0,1); // move to the begining of the second line
      if (gameLineNum != 0){
        lcd.print(" Right = Info   ");
        gameLineNum = 0;
      } else {
        lcd.print(" Select = Start ");
        gameLineNum = 1;
      }
      gameDelay = 0;
      while (gameDelay < 2000){
       // Add a delay in the loop.
       delay(1);
       gameDelay++;
       lcd_key = read_LCD_buttons(); // read the buttons
  
       switch (lcd_key){ // depending on which button was pushed, we perform an action
    
           case btnRIGHT:{
                gameState = 1;
                gameLineNum = 0;
                break;
           }
           case btnSELECT:{
                 gameState = 3;
                 gameLineNum = 0;
                 break;
           }
       }
      }
  } // End gameState 0, asteroids start screen.

    if (gameState == 1) { // Credits state.
      // The player wants to know who made this silly game. Let's tell them.
    lcd.setCursor(0,0);             // set the LCD cursor   position 
    lcd.print(" Created By the ");
    lcd.setCursor(0,1);             // move to the begining of the second line
    lcd.print("AlaskaLinuxUser!");
    
      
      if (gameLineNum > 2) {
        lcd.setCursor(0,0);             // set the LCD cursor   position 
        lcd.print("Right = How to  ");
        lcd.setCursor(0,1);             // move to the begining of the second line
        lcd.print("Left = Main Menu");
        gameLineNum = 0;
      } else if (gameLineNum == 2) {
        lcd.setCursor(0,0);             // set the LCD cursor   position 
        lcd.print("ress.com        ");
        lcd.setCursor(0,1);             // move to the begining of the second line
        lcd.print(" Select = Start ");
        gameLineNum = 3;
      } else if (gameLineNum == 1) {
        lcd.setCursor(0,0);             // set the LCD cursor   position 
        lcd.print("https://thealask");
        lcd.setCursor(0,1);             // move to the begining of the second line
        lcd.print("alinuxuser.wordp");
        gameLineNum = 2;
      } else if (gameLineNum == 0){
        lcd.setCursor(0,0);             // set the LCD cursor   position 
        lcd.print(" Created By the ");
        lcd.setCursor(0,1);             // move to the begining of the second line
        lcd.print("AlaskaLinuxUser!");
        gameLineNum = 1;
      }
      gameDelay = 0;
      while (gameDelay < 2000){
       // Add a delay in the loop.
       delay(1);
       gameDelay++;
     lcd_key = read_LCD_buttons();   // read the buttons
  
       switch (lcd_key){ // depending on which button was pushed, we perform an action
    
           case btnRIGHT:{
                 gameState = 2;
                 gameLineNum = 0;
                break;
           }
           case btnLEFT:{
                 gameState = 0;
                 gameLineNum = 0;
                 break;
           }
           case btnSELECT:{
                 gameState = 3;
                 gameLineNum = 0;
                 break;
           }
       }
       }
  } // End gameState 1, credits.

    if (gameState == 2) { // How to info state.
      // The player wants to know how to play, let's tell them.
        lcd.setCursor(0,0); // set the LCD cursor   position 
        lcd.print("Avoid asteroids ");
        lcd.setCursor(0,1); // move to the begining of the second line
        lcd.print("by up/down bttns");
    
      
      if (gameLineNum > 2) {
        lcd.setCursor(0,0);             // set the LCD cursor   position 
        lcd.print(" Select = Start ");
        lcd.setCursor(0,1);             // move to the begining of the second line
        lcd.print(" Left = credits ");
        gameLineNum = 0;
      } else if (gameLineNum == 2) {
        lcd.setCursor(0,0);             // set the LCD cursor   position 
        lcd.print("Select to go to ");
        lcd.setCursor(0,1);             // move to the begining of the second line
        lcd.print("main menu screen");
        gameLineNum = 3;
      } else if (gameLineNum == 1) {
        lcd.setCursor(0,0);             // set the LCD cursor   position 
        lcd.print("Right will shoot");
        lcd.setCursor(0,1);             // move to the begining of the second line
        lcd.print("Left will pause ");
        gameLineNum = 2;
      } else if (gameLineNum == 0){
        lcd.setCursor(0,0);             // set the LCD cursor   position 
        lcd.print("Avoid asteroids ");
        lcd.setCursor(0,1);             // move to the begining of the second line
        lcd.print("by up/down bttns");
        gameLineNum = 1;
      }
      gameDelay = 0;
      while (gameDelay < 2000){
       // Add a delay in the loop.
       delay(1);
       gameDelay++;
     lcd_key = read_LCD_buttons();   // read the buttons
  
       switch (lcd_key){ // depending on which button was pushed, we perform an action
    
           case btnLEFT:{
                 gameState = 1;
                 gameLineNum = 0;
                 break;
           }
           case btnSELECT:{
                 gameState = 3;
                 gameLineNum = 0;
                 break;
           }
       }
       }
  } // End gameState 2, How to info.

    if (gameState == 3) { // Play state.
      // The player has entered play mode.

       gameTime++;
       if (gameTime > 1){
        gameTime = 0;
        gamePaused = false;
       }

       gameShoot = true; // you have not shot yet this round.
       gameAsteroidOne--; // First asteroid starting space.
       gameAsteroidTwo--; // Second asteroid starting space.
       gameAsteroidThree--; // Third asteroid starting space.
       gameAsteroidFour--; // Fourth Asteroid starting space.

    if (gameAsteroidOne == -1) {
      if (gamePosition == 0) {
        // Game over, you lose.
           gameState = 6;
           gameLineNum = 0;
      } else {
        gameScore++; // get a point!
        gameSpeed = set_gameSpeed(gameScore);
        gameAsteroidOne = get_random(4);
      }
    } // end gameAsteroidOne.

    if (gameAsteroidTwo == -1) {
      if (gamePosition == 0) {
        // Game over, you lose.
           gameState = 6;
           gameLineNum = 0;
      } else {
        gameScore++; // get a point!
        gameSpeed = set_gameSpeed(gameScore);
        gameAsteroidTwo = get_random(2);
      }
    } // end gameAsteroidTwo.

    if (gameAsteroidThree == -1) {
      if (gamePosition == 1) {
        // Game over, you lose.
           gameState = 6;
           gameLineNum = 0;
      } else {
        gameScore++; // get a point!
        gameSpeed = set_gameSpeed(gameScore);
        gameAsteroidThree = get_random(3);
      }
    } // end gameAsteroidThree.

    if (gameAsteroidFour == -1) {
      if (gamePosition == 1) {
        // Game over, you lose.
           gameState = 6;
           gameLineNum = 0;
      } else {
        gameScore++; // get a point!
        gameSpeed = set_gameSpeed(gameScore);
        gameAsteroidFour = get_random(1);
      }
    } // end gameAsteroidFour.

    if (gameScore > 99){
      // You win! Great job!
      gameState = 5;
      gameLineNum = 0;
    }

    // Set up our display.
      // The space ship
      String shipUpper = " ";
      String shipLower = " ";
      if (gamePosition == 0){
        shipUpper = ">";
        shipLower = " ";
        } else {
            shipUpper = " ";
            shipLower = ">";
          }

          // Upper blocks.
          lcd.setCursor(0,0);
          lcd.print("00");
          lcd.setCursor(2,0);
          lcd.print(gameBullets);
          lcd.setCursor(3,0);
          lcd.print("|");
          lcd.setCursor(4,0);
          lcd.print(shipUpper);

          for (int a = 5; a < 17; a++){
            lcd.setCursor(a,0);
            if (a == gameAsteroidOne+5){
              lcd.print("*");
            } else if (a == gameAsteroidTwo+5){
              lcd.print("*");
            } else {
              lcd.print(" ");
            }
          }

          // Lower blocks.
          lcd.setCursor(0,1);
          lcd.print(gameScore);
            if (gameScore < 10) {
              lcd.setCursor(1,1);
              lcd.print("  ");
            } else if (gameScore < 100) {
              lcd.setCursor(2,1);
              lcd.print(" ");
            }
            
          lcd.setCursor(3,1);
          lcd.print("|");
          lcd.setCursor(4,1);
          lcd.print(shipLower);

          for (int a = 5; a < 17; a++){
            lcd.setCursor(a,1);
            if (a == gameAsteroidThree+5){
              lcd.print("*");
            } else if (a == gameAsteroidFour+5){
              lcd.print("*");
            } else {
              lcd.print(" ");
            }
          }
    
    gameDelay = 0;
      while (gameDelay < 1000){
       // Add a delay in the loop.
       delay(1);
       gameDelay+=gameSpeed;
     lcd_key = read_LCD_buttons(); // read the buttons

     switch (lcd_key){
  
         case btnRIGHT:{
                // Shoot command
                if (gameBullets > 0 && gameShoot) {
                  if (gamePosition == 0){
                    lcd.setCursor(5,0);
                    lcd.print("------------");
                    gameBullets--;
                    gameShoot = false; // You already shot.
                    gameAsteroidOne = get_random(4);
                    gameAsteroidTwo = get_random(2);
                  } else {
                    lcd.setCursor(5,1);
                    lcd.print("------------");
                    gameBullets--;
                    gameShoot = false; // You already shot.
                    gameAsteroidThree = get_random(3);
                    gameAsteroidFour = get_random(1);
                  }
                }
                break;
         }
         case btnLEFT:{
                 if (!gamePaused) {
                 gameState = 4;
                 gameLineNum = 0;
                 gamePaused = true;
                 }
                 break;
         }    
         case btnUP:{
                 gamePosition = 0;
                 break;
         }
         case btnDOWN:{
                 gamePosition = 1;
                 break;
         }
         case btnSELECT:{
                 gameState = 0;
                 gameLineNum = 0;
                 break;
         }
     }   
    }  
  } // End gameState 3, play state.

    if (gameState == 4) { // Pause state.
      // The player paused the game. 
    lcd_key = read_LCD_buttons();   // read the buttons
  
     switch (lcd_key){
      
         case btnLEFT:{
                 gameState = 3;
                 gameLineNum = 0;
                 break;
         }
         case btnSELECT:{
                 gameState = 0;
                 gameLineNum = 0;
                 break;
         }
     }
  } // End gameState 4, pause state.

    if (gameState == 5) { // Win state.
      // The player has won the game, let's tell them.
        lcd.setCursor(0,0); // set the LCD cursor   position 
        lcd.print("Asteroids Game! ");
        lcd.setCursor(0,1); // move to the begining of the second line
        lcd.print("    YOU WIN     ");
    
      
      if (gameLineNum == 2) {
        lcd.setCursor(0,0); // set the LCD cursor   position 
        lcd.print(" Select for Menu");
        lcd.setCursor(0,1); // move to the begining of the second line
        lcd.print("Asteroids Game! ");
        gameLineNum = 0;
      } else if (gameLineNum == 1) {
        lcd.setCursor(0,0); // set the LCD cursor   position 
        lcd.print("    YOU WIN     ");
        lcd.setCursor(0,1); // move to the begining of the second line
        lcd.print(" Select for Menu");
        gameLineNum = 2;
      } else if (gameLineNum == 0){
        lcd.setCursor(0,0); // set the LCD cursor   position 
        lcd.print("Asteroids Game! ");
        lcd.setCursor(0,1); // move to the begining of the second line
        lcd.print("    YOU WIN     ");
        gameLineNum = 1;
      }
      gameDelay = 0;
      while (gameDelay < 2000){
       // Add a delay in the loop.
       delay(1);
       gameDelay++;
     lcd_key = read_LCD_buttons();   // read the buttons
  
       switch (lcd_key){ // depending on which button was pushed, we perform an action
    
           case btnSELECT:{
                 gameState = 0;
                 gameLineNum = 0;
                 break;
           }
       }
       }
  } // End gameState 5, win state.

    if (gameState == 6) { // Lose state.
      // The player has lost the game. Let's tell them.
     lcd.setCursor(0,0); // set the LCD cursor   position 
        lcd.print("Asteroids Game! ");
        lcd.setCursor(0,1); // move to the begining of the second line
        lcd.print("    YOU LOSE    ");
    
      
      if (gameLineNum == 2) {
        lcd.setCursor(0,0); // set the LCD cursor   position 
        lcd.print("    YOU LOSE    ");
        lcd.setCursor(0,1); // move to the begining of the second line
        lcd.print("Asteroids Game! ");
        gameLineNum = 0;
      } else if (gameLineNum == 1) {
        lcd.setCursor(0,0); // set the LCD cursor   position 
        lcd.print("    YOU LOSE    ");
        lcd.setCursor(0,1); // move to the begining of the second line
        lcd.print(" Select for Menu");
        gameLineNum = 2;
      } else if (gameLineNum == 0){
        lcd.setCursor(0,0); // set the LCD cursor   position 
        lcd.print("Asteroids Game! ");
        lcd.setCursor(0,1); // move to the begining of the second line
        lcd.print("    YOU LOSE    ");
        gameLineNum = 1;
      }
      gameDelay = 0;
      while (gameDelay < 2000){
       // Add a delay in the loop.
       delay(1);
       gameDelay++;
     lcd_key = read_LCD_buttons();   // read the buttons
  
       switch (lcd_key){ // depending on which button was pushed, we perform an action
    
           case btnSELECT:{
                 gameState = 0;
                 gameLineNum = 0;
                 break;
           }
       }
       }
  } // End gameState 6, Lose state.
  
}
