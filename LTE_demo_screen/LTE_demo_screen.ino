/*  Copyright 2019 by AlaskaLinuxUser (https://thealaskalinuxuser.wordpress.com)
*
*   Licensed under the Apache License, Version 2.0 (the "License");
*   you may not use this file except in compliance with the License.
*   You may obtain a copy of the License at
*
*       http://www.apache.org/licenses/LICENSE-2.0
*
*   Unless required by applicable law or agreed to in writing, software
*   distributed under the License is distributed on an "AS IS" BASIS,
*   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*   See the License for the specific language governing permissions and
*   limitations under the License.
*/

/* 
 *  This example tests the SIMcom 7000A shield with a Nokia 5110 display. - AlaskaLinuxUser 20190430
 *  I have modified the PCD8544.h file as follows:
 *          PCD8544(uint8_t sclk  = 3,   // clock       (display pin 2)
 *              uint8_t sdin  = 4,   // data-in     (display pin 3) 
 *              uint8_t dc    = 5,   // data select (display pin 4) 
 *              uint8_t reset = 12,   // reset       (display pin 8)
 *              uint8_t sce   = 13);  // enable      (display pin 5)
 *  
 *  I also use the following other pins:
 *    Pin A0-A5 for buttons 1 through 6.
 *    Any ground pin for buttons 1 through 6 (pull up resistor as well).
 *    The 3.3V power pin for buttons 1 through 6, and for the screen backlighting.
 */

/*  This is an example sketch to test the core functionalities of SIMCom-based cellular modules.
    This code supports the SIM7000-series modules (LTE/NB-IoT shields) for low-power IoT devices!

    Note: this code is specifically meant for AVR microcontrollers (Arduino Uno, Mega, Leonardo, etc)
    However, if you are using an ESP8266 please make the minor modifications mentioned below in the
    comments for the pin definitions and software serial initialization.

    For ESP32 please use the ESP32_LTE_Demo instead: https://github.com/botletics/SIM7000-LTE-Shield/blob/master/Code/examples/ESP32_LTE_Demo/ESP32_LTE_Demo.ino

    Author: Timothy Woo (www.botletics.com)
    Github: https://github.com/botletics/SIM7000-LTE-Shield
    Last Updated: 1/15/2018
    License: GNU GPL v3.0
*/

/******* ORIGINAL ADAFRUIT FONA LIBRARY TEXT *******/
/***************************************************
  This is an example for our Adafruit FONA Cellular Module

  Designed specifically to work with the Adafruit FONA
  ----> http://www.adafruit.com/products/1946
  ----> http://www.adafruit.com/products/1963
  ----> http://www.adafruit.com/products/2468
  ----> http://www.adafruit.com/products/2542

  These cellular modules use TTL Serial to communicate, 2 pins are
  required to interface
  Adafruit invests time and resources providing this open source code,
  please support Adafruit and open-source hardware by purchasing
  products from Adafruit!

  Written by Limor Fried/Ladyada for Adafruit Industries.
  BSD license, all text above must be included in any redistribution
 ****************************************************/

#include "Adafruit_FONA.h"

#define SIMCOM_7000 // SIM7000A/C/E/G

// For SIM7000 shield
#define FONA_PWRKEY 6
#define FONA_RST 7
//#define FONA_DTR 8 // Connect with solder jumper
//#define FONA_RI 9 // Need to enable via AT commands
#define FONA_TX 10 // Microcontroller RX
#define FONA_RX 11 // Microcontroller TX

// this is a buffer for replies
char replybuffer[10];

#include <SoftwareSerial.h>
SoftwareSerial fonaSS = SoftwareSerial(FONA_TX, FONA_RX);

SoftwareSerial *fonaSerial = &fonaSS;
  
// Use this one for LTE CAT-M/NB-IoT modules (like SIM7000)
  Adafruit_FONA_LTE fona = Adafruit_FONA_LTE();

uint8_t readline(char *buff, uint8_t maxbuff, uint16_t timeout = 0);
char messageText[120];
char sendText[21];
int smsNum = 1;
bool GPSon = 0;

  //*********************** Screen ******************
#include <PCD8544.h>
// A custom glyph (Network Status)...
static const byte glyphN[] = { 0xe0, 0x00, 0xf8, 0x00, 0xfe };
int screenNumber = 1; // The menu screen number.
int menuNumber = 285; // The selected menu item.
int updateScreen = 0; // A counter to update the screen.
int numLetter = 0;
int curPos = 0;
static PCD8544 lcd;
  //*********************** Screen ******************

// define some values for the buttons
int lcd_key     = 0;
int adc_key_in  = 0;

#define btnRIGHT  0
#define btnLEFT     1
#define btnDOWN   2
#define btnUP   3
#define btnSELECT 4
#define btnBACK   5
#define btnNONE   6

int read_LCD_buttons(){               // read the buttons
  adc_key_in = 0;
    adc_key_in = analogRead(A0);       // read the value from the sensor 
    //Serial.print("anolog0:"); Serial.println(adc_key_in);
    if (adc_key_in > 550)   return btnRIGHT;
    adc_key_in = analogRead(A1);       // read the value from the sensor 
    //Serial.print("anolog1:"); Serial.println(adc_key_in);
    if (adc_key_in > 550)   return btnLEFT;
    adc_key_in = analogRead(A2);       // read the value from the sensor 
    //Serial.print("anolog2:"); Serial.println(adc_key_in);
    if (adc_key_in > 550)   return btnDOWN;
    adc_key_in = analogRead(A3);       // read the value from the sensor 
    //Serial.print("anolog3:"); Serial.println(adc_key_in);
    if (adc_key_in > 550)   return btnUP;
    adc_key_in = analogRead(A4);       // read the value from the sensor 
    //Serial.print("anolog4:"); Serial.println(adc_key_in);
    if (adc_key_in < 750 && adc_key_in > 550)   return btnSELECT;
    adc_key_in = analogRead(A5);       // read the value from the sensor 
    //Serial.print("anolog5:"); Serial.println(adc_key_in);
    if (adc_key_in > 550)   return btnBACK; 
    return btnNONE;                // when all others fail, return this.
}


void setup() {
  // TESTING ONLY // Not a valid number.
  messageText[0] = 'T';messageText[1] = 'E';messageText[2] = 'S';messageText[3] = 'T';
  sendText[0] = '1';sendText[1] = '9';sendText[2] = '0';sendText[3] = '7';sendText[4] = '8';sendText[5] = '6';
  sendText[6] = '7';sendText[7] = '5';sendText[8] = '3';sendText[9] = '0';sendText[10] = '9';
  // TESTING ONLY //
  //*********************** Screen ******************
  // PCD8544-compatible displays may have a different resolution...
  lcd.begin(84, 48);
  //lcd.setContrast(90); // Adjust the screen contrast.
  // Add the glyph to the ASCII table...
  lcd.createChar(0, glyphN);
  //*********************** Screen ******************

  pinMode(FONA_RST, OUTPUT);
  digitalWrite(FONA_RST, HIGH); // Default state

  pinMode(FONA_PWRKEY, OUTPUT);

  powerOn(); // See function definition at the very end of the sketch

  Serial.begin(9600);
  Serial.println(F("Initializing"));
  
  fonaSS.begin(115200); // Default SIM7000 shield baud rate
  fonaSS.println("AT+IPR=9600"); // Set baud rate
  delay(100); // Short pause to let the command run
  fonaSS.begin(9600);
  if (! fona.begin(fonaSS)) {
    Serial.println(F("F"));
    while (1); // Don't proceed if it couldn't find the device
  }

  // Set modem to full functionality
  fona.setFunctionality(1); // AT+CFUN=1

  //fona.setNetworkSettings(F("your APN"), F("your username"), F("your password"));
  //fona.setNetworkSettings(F("m2m.com.attz")); // For AT&T IoT SIM card
  fona.setNetworkSettings(F("hologram")); // For Hologram SIM card
  // WJH fona.setNetworkSettings(F("fp.com.attz")); // For Freedom Pop SIM card - Sort of works. Connects, but you need their app to text/call/sms/web/etc.
  // WJH fona.setNetworkSettings(F("tfdata")); // For Straight Talk SIM card - sort of works. Connects and sends/receives SMS, but no web data.
  
  
  // Optionally configure HTTP gets to follow redirects over SSL.
  // Default is not to follow SSL redirects, however if you uncomment
  // the following line then redirects over SSL will be followed.
  //fona.setHTTPSRedirect(true);

  // Other examples of some things you can set:
  fona.setPreferredMode(38); // Use LTE only, not 2G
  fona.setPreferredLTEMode(1); // Use LTE CAT-M only, not NB-IoT
  fona.setOperatingBand("CAT-M", 12); // AT&T uses band 12
  fona.enableRTC(true);
  
  //fona.enableSleepMode(true);
  //fona.set_eDRX(1, 4, "0010");
  //fona.enablePSM(true);
  //fona.setNetLED(true, 2, 64, 3000); // on/off, mode, timer_on, timer_off
  
  printMenu();
}

void printMenu(void) {
  Serial.println(F("Print [m]enu"));
  Serial.println(F("Read SIM [C]CID"));
  Serial.println(F("[U]nlock SIM"));
  Serial.println(F("Network in[f]o"));
  Serial.println(F("[N]umber of SMS's"));
  Serial.println(F("[r]ead SMS #"));
  Serial.println(F("[d]elete SMS #"));
  Serial.println(F("[s]end SMS"));
  Serial.println(F("Network [t]ime")); // Works just by being connected to network
  Serial.println(F("[G] Enable cellular data"));
  Serial.println(F("[g] Disable cellular data"));
  Serial.println(F("Read [w]ebpage"));
  Serial.println(F("Post to [W]ebsite"));
  Serial.println(F("GPS [O]n"));
  Serial.println(F("GPS [o]ff"));
  Serial.println(F("[L]ocation"));
  Serial.println(F("[S]erial tunnel"));
  Serial.println(F(""));
}

void loop() {

    //Get the time.
     char buffer[23];
     fona.getTime(buffer, 23);
     char letterNumbers[91] = {'0','1','2','3','4','5','6','7','8','9',' ','A','B','C','D','E','F','G',
  'H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','a','b','c','d','e',
  'f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z','`','~','!',
  '@','#','$','%','^','&','*','(',')','_','+','|','-','=',';',':',',','/','.','<','>','?'};

  //*********************** Status Bar ******************
  // Build the information bar.
  if (updateScreen == 0) {
  // Will look like this: Batt% GPS (indicator) Network (indicator) #SMS (messages)
  lcd.setCursor(0, 0);
  // Get the battery percentage:
  uint16_t vbat;
  if (! fona.getBattPercent(&vbat)) {
            Serial.println(F("F"));
            lcd.print("**% ");
          } else {
            Serial.print(F("VPct = ")); Serial.print(vbat); Serial.println(F("%"));
            lcd.print(vbat); lcd.print("% ");
          }

  // Get the GPS status:
  if (GPSon == 1) {lcd.print("GPS ");} else {lcd.print("--- ");} 

  // Get the network status:
  uint8_t n = fona.getNetworkStatus();
        Serial.print(F("Net stat "));
        Serial.print(n);
        Serial.print(F(": "));
        if (n == 0) {Serial.println(F("Not registered")); lcd.print("X");}
        if (n == 1) {Serial.println(F("Home")); lcd.write(0);}
        if (n == 2) {Serial.println(F("Searching")); lcd.print("S");}
        if (n == 3) {Serial.println(F("Denied")); lcd.print("D");}
        if (n == 4) {Serial.println(F("Unknown")); lcd.print("?");}
        if (n == 5) {Serial.println(F("Roaming")); lcd.write(0);}
        lcd.print(" ");

  // Get the number of SMS messages:
  int8_t smsnum = fona.getNumSMS();
        if (smsnum < 0) {
          Serial.println(F("F"));
          lcd.print("-0- ");
        } else {
          Serial.print(smsnum); Serial.println(F(" SMS's"));
          lcd.println(smsnum);
        }
        Serial.print(F("> "));
        updateScreen++;
  } else if (updateScreen == 30) {
    updateScreen = -1;

  } else {
    updateScreen++;
    delay(1); 
  }
  //*********************** Status Bar ******************
  
  if (Serial.available() > 0) {
    checkSerialConn();
  }

  //*********************** Screen updates ******************
  // Clear the display....
                lcd.setCursor(0, 1);
                lcd.print("              ");
                lcd.setCursor(0, 2);
                lcd.print("              ");
                lcd.setCursor(0, 3);
                lcd.print("              ");
                lcd.setCursor(0, 4);
                lcd.print("              ");
                lcd.setCursor(0, 5);
                lcd.print("              ");
                
  switch (screenNumber){
    case 1:{ // Main screen
                lcd.setCursor(0, 1);
                lcd.print("1:TEXT");
                lcd.setCursor(0, 2);
                lcd.print("2:GPS");
                lcd.setCursor(0, 3);
                lcd.print("3:PROGRAMS");
                lcd.setCursor(0, 4);
                lcd.print("    ");lcd.print(buffer);
                break;
    }
    case 2:{ // Location screen
                lcd.setCursor(0, 1);
                if (GPSon == 1) {
                fona.getGPS(0, messageText, 120);
                lcd.print(messageText);} else { lcd.print("Turn on GPS.");}
                lcd.setCursor(0, 5);
                lcd.print("1:SEND LOCATION");
                break;
    }
    case 3:{ // GPS screen
                lcd.setCursor(0, 1);
                lcd.print("1:VIEW LOCATION");
                lcd.setCursor(0, 2);
                lcd.print("2:COMPASS");
                lcd.setCursor(0, 3);
                lcd.print("3:GPS ON");
                lcd.setCursor(0, 4);
                lcd.print("4:GPS OFF");
                break;
    }
    case 4:{ // Compass screen
                float heading;
                if (fona.getGPS(&heading, &heading, &heading, &heading)) {
                  lcd.setCursor(0, 1);
                  lcd.print("Heading: "); lcd.print(heading);
                }
                lcd.setCursor(0, 4);
                lcd.print("4:MAIN MENU");
                break;
    }
    case 5:{ // Phone send number screen.
                lcd.setCursor(0, 1);
                lcd.print("SEND TO: ");
                lcd.setCursor(0, 2);
                lcd.print(sendText);
                lcd.setCursor(curPos*6, 2);
                lcd.print(letterNumbers[numLetter]);
                lcd.setCursor(0, 5);
                lcd.print("SELECT SENDS");
                break;
    }
    case 6:{ // SMS screen
                lcd.setCursor(0, 1);
                lcd.print("1:READ SMS");
                lcd.setCursor(0, 2);
                lcd.print("2:SEND SMS");
                lcd.setCursor(0, 3);
                lcd.print("3:DELETE ALL SMS");
                lcd.setCursor(0, 4);
                lcd.print(" ");
                break;
    }
    case 7:{ // Type message screen
                lcd.setCursor(0, 1);
                lcd.print("TYPE MESSAGE:");
                lcd.setCursor(0, 2);
                lcd.print(messageText);
                lcd.setCursor(curPos*6, 2);
                lcd.print(letterNumbers[numLetter]);
                lcd.setCursor(0, 5);
                lcd.print("SELECT SENDS");
                break;
    }
    case 8:{ // Delete all messages screen.
                lcd.setCursor(0, 1);
                lcd.print("DELETE ALL");
                lcd.setCursor(0, 2);
                lcd.print("MESSAGES?");
                lcd.setCursor(0, 3);
                lcd.print("  3:NO");
                lcd.setCursor(0, 4);
                lcd.print("  4:YES");
                break;
    }
    case 9:{ // View all messages screen
                lcd.setCursor(0, 1);
                lcd.print("READ WHICH MESSAGE?");
                lcd.setCursor(0, 3);
                lcd.print(smsNum);
                lcd.setCursor(0, 5);
                lcd.print("SELECT READS");
                break;
    }
    case 10:{ // Read message screen
                lcd.setCursor(0, 1);
                lcd.print(sendText);
                lcd.setCursor(0, 2);
                lcd.print(messageText);
                lcd.setCursor(0, 4);
                lcd.print("3:REPLY");
                lcd.setCursor(0, 5);
                lcd.print("4:DELETE");
                break;
    }
    case 11:{ // Delete message screen
                lcd.setCursor(0, 1);
                lcd.print("DELETE");
                lcd.setCursor(0, 2);
                lcd.print("MESSAGE?");
                lcd.setCursor(0, 3);
                lcd.print("  3:NO");
                lcd.setCursor(0, 4);
                lcd.print("  4:YES");
                break;
    }
    case 12:{ // Programs screen
                lcd.setCursor(0, 1);
                lcd.print("1:LINEMODE BROWSER");
                lcd.setCursor(0, 2);
                lcd.print("2:GAMES");
                lcd.setCursor(0, 3);
                lcd.print("3:SETTINGS");
                break;
    }
    case 13:{ // Settings screen
                lcd.setCursor(0, 1);
                lcd.print("1:NETWORK");
                lcd.setCursor(0, 2);
                lcd.print("2:SCREEN");
                break;
    }
    case 14:{ // Network Settings screen
                lcd.setCursor(0, 1);
                lcd.print("1:DATA ON");
                lcd.setCursor(0, 2);
                lcd.print("2:DATA OFF");
                lcd.setCursor(0, 3);
                lcd.print("3:NET INFO");
                break;
    }
    case 15:{ // Network info screen
              // read the RSSI
              uint8_t n = fona.getRSSI();
              int8_t r;
      
              Serial.print(F("RSSI = ")); Serial.print(n); Serial.print(": ");
              if (n == 0) r = -115;
              if (n == 1) r = -111;
              if (n == 31) r = -52;
              if ((n >= 2) && (n <= 30)) {
                r = map(n, 2, 30, -110, -54);
              }
              fona.getNetworkInfo();
                lcd.setCursor(0, 1);
                lcd.print("SIGNAL STRENGTH:");
                lcd.setCursor(0, 3);
                lcd.print(r);
                lcd.setCursor(0, 4);
                lcd.print("4:MAIN MENU");
                break;
    }
  }
  
  //*********************** Screen updates ******************

  //*********************** Button Reading ******************
  int waiting = 0;
  menuNumber = 0;
  while ( waiting < 30 ){
    waiting++;
    delay(10);
    lcd_key = read_LCD_buttons(); // read the buttons
    // TESTING ONLY // Serial.print(lcd_key);
  switch (lcd_key){
  
         case btnUP:{
                menuNumber = (1+screenNumber)*screenNumber;
                break;
         }
         case btnDOWN:{
                menuNumber = (2+screenNumber)*screenNumber;
                break;
         }   
         case btnLEFT:{
                menuNumber = (3+screenNumber)*screenNumber;
                break;
         }
         case btnRIGHT:{
                menuNumber = (4+screenNumber)*screenNumber;
                break;
         }
         case btnSELECT:{
                menuNumber = 255;
                break;
         }
         case btnBACK:{
                menuNumber = 256;
                break;
         }
     }
     // TESTING ONLY // Serial.print(menuNumber);
  }

  //*********************** Button Reading ******************

  //*********************** Menu Options ******************
  // TESTING ONLY //  Serial.println(menuNumber);
  // TESTING ONLY //  Serial.println(curPos);
  // TESTING ONLY //  Serial.println(screenNumber);
  switch (menuNumber){
  
         case 2:{ // Screen 1 - Main screen.
                screenNumber = 6;
                break;
         }
         case 3:{
                screenNumber = 3;
                break;
         }   
         case 4:{
                screenNumber = 12;
                break;
         }
         case 5:{
                screenNumber = 1;
                break;
         }
         case 6:{ // Screen 2 - Location screen.
                screenNumber = 5;
                break;
         }
         case 12:{ 
                screenNumber = 2;
                break;
         }   
         case 15:{ // Screen 3 - GPS screen.
                screenNumber = 4;
                break;
         }
         case 18:{
            // turn GPS on
            GPSon=1;
            if (!fona.enableGPS(true))
            Serial.println(F("F"));
                break;
         }
         case 21:{
            // turn GPS off
            GPSon=0;
            if (!fona.enableGPS(false))
            Serial.println(F("F"));
                break;
         }
         case 32:{ // Screen 4 - Compass screen.
                screenNumber = 1;
                break;
         }
         case 30:{ // Screen 5 - Enter phone number screen.
                // Up one character
                if (numLetter >= 90){
                  numLetter = 0;
                } else {
                  numLetter++;
                }
                break;
         }
         case 35:{
                // Down one character
                if (numLetter <= 0){
                  numLetter = 90;
                } else {
                  numLetter--;
                }
                break;
         }   
         case 40:{
                // Left one position
                if (curPos <= 0){
                  curPos = 0;
                } else {
                  sendText[curPos] = letterNumbers[numLetter];
                  curPos--;
                }
                break;
         }
         case 45:{
                // Right one position
                if (curPos >= 20){
                  curPos = 20;
                } else {
                  sendText[curPos] = letterNumbers[numLetter];
                  curPos++;
                }
                break;
         }
         case 42:{ // Screen 6 - Text/SMS screen.
                screenNumber = 9;
                break;
         }
         case 48:{
                screenNumber = 7;
                break;
         }   
         case 54:{
                screenNumber = 8;
                break;
         }
         case 56:{ // Screen 7 - Type message screen.
                // Up one character
                if (numLetter >= 90){
                  numLetter = 0;
                } else {
                  numLetter++;
                }
                break;
         }
         case 63:{
                // Down one character
                if (numLetter <= 0){
                  numLetter = 90;
                } else {
                  numLetter--;
                }
                break;
         }   
         case 70:{
                // Left one position
                if (curPos <= 0){
                  curPos = 0;
                } else {
                  messageText[curPos] = letterNumbers[numLetter];
                  curPos--;
                }
                break;
         }
         case 77:{
                // Right one position
                if (curPos >= 119){
                  curPos = 119;
                } else {
                  messageText[curPos] = letterNumbers[numLetter];
                  curPos++;
                }
                screenNumber = 7;
                break;
         }
         case 88:{ // Screen 8 - Delete ALL message screen.
                screenNumber = 6;
                break;
         }
         case 96:{
              for (int deleteALL = 32; deleteALL >= 0; deleteALL--) {
                if (fona.deleteSMS(deleteALL)) {
                Serial.println(F("K"));
                } else {
                Serial.println(F("F"));
                }}
                screenNumber = 6;
                break;
         }
         case 90:{ // Screen 9 - Scroll message screen.
                // Up one number
                smsNum++;
                break;
         }
         case 99:{
                // Down one number
                if (smsNum <= 1) {
                  smsNum = 1;
                } else {
                  smsNum--;
                }
                break;
         }   
         case 108:{
                // Up ten number
                smsNum+=10;
                break;
         }
         case 117:{
                // Down ten number
                if (smsNum <= 11) {
                  smsNum = 1;
                } else {
                  smsNum-=10;
                }
                break;
         }
         case 130:{ // Screen 10 - View message screen.
                screenNumber = 7;
                break;
         }
         case 140:{
                screenNumber = 11;  // TODO: Add message number.
                break;
         }
         case 154:{ // Screen 11 - delete message screen. // TODO: Add message number.
                screenNumber = 6;
                break;
         }
         case 165:{
                // delete an SMS
                  if (fona.deleteSMS(smsNum-1)) {
                    Serial.println(F("K"));
                  } else {
                    Serial.println(F("F"));
                  }
                screenNumber = 6;
                break;
         }
         case 156:{ // Screen 12 - Programs screen.
                screenNumber = 1;
                break;
         }
         case 168:{
                screenNumber = 1;
                break;
         }
         case 180:{
                screenNumber = 13;
                break;
         }
         case 182:{ // Screen 13 - Settings screen.
                screenNumber = 14;
                break;
         }
         case 195:{
                screenNumber = 1;
                break;
         }
         case 210:{ // Screen 14 - Network Settings screen.
                // turn GPRS on
                if (!fona.enableGPRS(true))
                Serial.println(F("F"));
                break;
         }
         case 224:{
                // turn GPRS off
                if (!fona.enableGPRS(false))
                Serial.println(F("F"));
                break;
         }
         case 238:{
                screenNumber = 15;
                break;
         }
         
         case 285:{ // Screen 15 - Network info.
                screenNumber = 1;
                break;
         }
         case 255:{ // Select Button
                if (screenNumber == 5) {
                  if (!fona.sendSMS(sendText, messageText)) {
                    Serial.println(F("F"));
                  } else {
                    Serial.println(F("Sent"));
                  }
                  screenNumber = 6;
                  curPos = 0;
                } else if (screenNumber == 7) {
                  screenNumber = 5;
                  curPos = 0;
                } else if (screenNumber == 9) {
                  // Retrieve SMS sender address/phone number.
                    if (! fona.getSMSSender(smsNum-1, sendText, 250)) {
                      Serial.println("F");
                      break;
                    }
                    // Retrieve SMS value.
                    uint16_t smslen;
                    if (! fona.readSMS(smsNum-1, messageText, 250, &smslen)) { // pass in buffer and max len!
                      Serial.println("F");
                      break;
                    }
                  screenNumber = 10;
                  curPos = 0;
                } else {
                  screenNumber = screenNumber;
                  // Essentially, do nothing.
                }
                break;
         }
         case 256:{ // Screen 15 - Network info.
                screenNumber = 1;
                break;
         }

         // That is screen 1 through 15 buttons.
         
     }


  //*********************** Menu Options ******************

  
} // End loop

void checkSerialConn() {
    Serial.print(F("FONA> "));
  while (! Serial.available() ) {
    if (fona.available()) {
      Serial.write(fona.read());
    }
  }

  char command = Serial.read();
  Serial.println(command);


  switch (command) {
    case 'm': {
        printMenu();
        break;
      }

    case 'U': {
        // Unlock the SIM with a PIN code
        char PIN[5];
        flushSerial();
        Serial.println(F("Enter 4-digit PIN"));
        readline(PIN, 3);
        Serial.println(PIN);
        Serial.print(F("Unlocking "));
        if (! fona.unlockSIM(PIN)) {
          Serial.println(F("F"));
        } else {
          Serial.println(F("K"));
        }
        break;
      }

    case 'C': {
        // read the CCID
        fona.getSIMCCID(replybuffer);  // make sure replybuffer is at least 21 bytes!
        Serial.print(F("SIM CCID = ")); Serial.println(replybuffer);
        break;
      }

    case 'f': {
        // Get connection type, cellular band, carrier name, etc.
        fona.getNetworkInfo();        
        break;
      }

    /*** SMS ***/
    case 'r': {
        // read an SMS
        flushSerial();
        Serial.print(F("Read #"));
        uint8_t smsn = readnumber();
        Serial.print(F("\n\rReading SMS #")); Serial.println(smsn);

        // Retrieve SMS sender address/phone number.
        if (! fona.getSMSSender(smsn, replybuffer, 250)) {
          Serial.println("F");
          break;
        }
        Serial.print(F("FROM: ")); Serial.println(replybuffer);

        // Retrieve SMS value.
        uint16_t smslen;
        if (! fona.readSMS(smsn, replybuffer, 250, &smslen)) { // pass in buffer and max len!
          Serial.println("F");
          break;
        }
        Serial.print(F("***** SMS #")); Serial.print(smsn);
        Serial.print(" ("); Serial.print(smslen); Serial.println(F(") bytes *****"));
        Serial.println(replybuffer);
        Serial.println(F("*****"));

        break;
      }

    case 'd': {
        // delete an SMS
        flushSerial();
        Serial.print(F("Delete #"));
        uint8_t smsn = readnumber();

        Serial.print(F("\n\rDeleting SMS #")); Serial.println(smsn);
        if (fona.deleteSMS(smsn)) {
          Serial.println(F("K"));
        } else {
          Serial.println(F("F"));
        }
        break;
      }

    case 's': {
        // send an SMS!
        char sendto[21], message[141];
        flushSerial();
        Serial.print(F("Send to #"));
        readline(sendto, 20);
        Serial.println(sendto);
        Serial.print(F("Type one-line message (140 char): "));
        readline(message, 140);
        Serial.println(message);
        if (!fona.sendSMS(sendto, message)) {
          Serial.println(F("F"));
        } else {
          Serial.println(F("Sent"));
        }

        break;
      }

    case 't': {
        // read the time
        char buffer[23];

        fona.getTime(buffer, 23);  // make sure replybuffer is at least 23 bytes!
        Serial.print(F("Time = ")); Serial.println(buffer);
        break;
      }


    /*********************************** GPS */

    case 'o': {
        // turn GPS off
        GPSon=0;
        if (!fona.enableGPS(false))
          Serial.println(F("F"));
        break;
      }
    case 'O': {
        // turn GPS on
        GPSon=1;
        if (!fona.enableGPS(true))
          Serial.println(F("F"));
        break;
      }

    case 'L': {
        
        // Uncomment this block if all you want to see is the AT command response
        // check for GPS location
        char gpsdata[120];
        fona.getGPS(0, gpsdata, 120);
        
          Serial.println(F("mode,fixstat,time(yyyymmddHHMMSS),lat,long,alt,spd,crse,fixmode,reserved1,HDOP,PDOP,VDOP,reserved2,view_sats,used_sats,reserved3,C/N0max,HPA,VPA"));
        
        Serial.println(gpsdata);

        break;
      }

    /*********************************** GPRS */

    case 'g': {
        // turn GPRS off
        if (!fona.enableGPRS(false))
          Serial.println(F("F"));
        break;
      }
    case 'G': {        
        // turn GPRS on
        if (!fona.enableGPRS(true))
          Serial.println(F("F"));
        break;
      }

#if !defined(SIMCOM_3G) && !defined(SIMCOM_7500)
    // The code below was written by Adafruit and only works on some modules
    case 'w': {
        // read website URL
        uint16_t statuscode;
        int16_t length;
        char url[80];

        flushSerial();
        Serial.println(F("URL to read (e.g. dweet.io/get/latest/dweet/for/sim7500test123):"));
        Serial.print(F("http://")); readline(url, 79);
        Serial.println(url);

        Serial.println(F("****"));
        if (!fona.HTTP_GET_start(url, &statuscode, (uint16_t *)&length)) {
          Serial.println("F");
          break;
        }
        while (length > 0) {
          while (fona.available()) {
            char c = fona.read();

            // Serial.write is too slow, we'll write directly to Serial register!
#if defined(__AVR_ATmega328P__) || defined(__AVR_ATmega168__)
            loop_until_bit_is_set(UCSR0A, UDRE0); /* Wait until data register empty. */
            UDR0 = c;
#else
            Serial.write(c);
#endif
            length--;
            if (! length) break;
          }
        }
        Serial.println(F("\n****"));
        fona.HTTP_GET_end();
        break;
      }

    case 'W': {
        // Post data to website
        uint16_t statuscode;
        int16_t length;
        char url[80];
        char data[80];

        flushSerial();
        Serial.println(F("NOTE: Use simple websites!"));
        Serial.println(F("URL (e.g. httpbin.org/post):"));
        Serial.print(F("http://")); readline(url, 79);
        Serial.println(url);
        Serial.println(F("Data to post (e.g. \"foo\" or \"{\"simple\":\"json\"}\"):"));
        readline(data, 79);
        Serial.println(data);

        Serial.println(F("****"));
        if (!fona.HTTP_POST_start(url, F("text/plain"), (uint8_t *) data, strlen(data), &statuscode, (uint16_t *)&length)) {
          Serial.println("Failed");
          break;
        }
        while (length > 0) {
          while (fona.available()) {
            char c = fona.read();

#if defined(__AVR_ATmega328P__) || defined(__AVR_ATmega168__)
            loop_until_bit_is_set(UCSR0A, UDRE0); /* Wait until data register empty. */
            UDR0 = c;
#else
            Serial.write(c);
#endif

            length--;
            if (! length) break;
          }
        }
        Serial.println(F("\n****"));
        fona.HTTP_POST_end();
        break;
      }
#endif

    /*****************************************/

    case 'S': {
        Serial.println(F("Creating SERIAL"));
        while (1) {
          while (Serial.available()) {
            delay(1);
            fona.write(Serial.read());
          }
          if (fona.available()) {
            Serial.write(fona.read());
          }
        }
        break;
      }

    default: {
        Serial.println(F("Unknown command"));
        printMenu();
        break;
      }

  // flush input
  flushSerial();
  while (fona.available()) {
    Serial.write(fona.read());
  }
  }// End serial available.
}

void flushSerial() {
  while (Serial.available())
    Serial.read();
}

char readBlocking() {
  while (!Serial.available());
  return Serial.read();
}
uint16_t readnumber() {
  uint16_t x = 0;
  char c;
  while (! isdigit(c = readBlocking())) {
    //Serial.print(c);
  }
  Serial.print(c);
  x = c - '0';
  while (isdigit(c = readBlocking())) {
    Serial.print(c);
    x *= 10;
    x += c - '0';
  }
  return x;
}

uint8_t readline(char *buff, uint8_t maxbuff, uint16_t timeout) {
  uint16_t buffidx = 0;
  boolean timeoutvalid = true;
  if (timeout == 0) timeoutvalid = false;

  while (true) {
    if (buffidx > maxbuff) {
      //Serial.println(F("SPACE"));
      break;
    }

    while (Serial.available()) {
      char c =  Serial.read();

      //Serial.print(c, HEX); Serial.print("#"); Serial.println(c);

      if (c == '\r') continue;
      if (c == 0xA) {
        if (buffidx == 0)   // the first 0x0A is ignored
          continue;

        timeout = 0;         // the second 0x0A is the end of the line
        timeoutvalid = true;
        break;
      }
      buff[buffidx] = c;
      buffidx++;
    }

    if (timeoutvalid && timeout == 0) {
      //Serial.println(F("TIMEOUT"));
      break;
    }
    delay(1);
  }
  buff[buffidx] = 0;  // null term
  return buffidx;
}

// Power on the module
void powerOn() {
  digitalWrite(FONA_PWRKEY, LOW);
  // See spec sheets for your particular module
  #if defined(SIMCOM_7000)
    delay(100); // For SIM7000
  #endif
  
  digitalWrite(FONA_PWRKEY, HIGH);
}
