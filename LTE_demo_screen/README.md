# LTE project (nickname: "Bone Phone")

You can read more about this project on my blog: https://thealaskalinuxuser.wordpress.com
Just search for "LTE project" to see how you can build this open source hardware texting phone for yourself!

This phone can send and receive text messages. Runs off of a 9 volt battery, and has six buttons.

# Operating instructions:

1. Turn on the Bone Phone by flipping the power switch to the "on" position.
2. Once it boots up, you will be presented with the main screen. On every screen, use the numbered button that corosponds to the menu item to select it.
3. Any screen that has you input text or numbers, use the buttons to navigate, 1 is up, to scroll through the numbers or letters, 2 is down, to scroll through the numbers and letters. Whatever letter or number you select is not saved to that spot until you use button 3 (left) or button 4 (right), at which point the letter/number is saved/entered.
4. Some screens tell you to push the select button (button 5) to do special functions. If needed, the screen will display the action you should take.
5. At any time, you can press the "home" button (button 6), which will take you back to the main menu.

# Features

- Send/receive text messages
- Turn on/off data or gps
- View GPS location and send as text
- Compass heading screen if GPS is on
- View network info.
- Using serial over USB, you can do many more features such as download web pages, upload to the web, and more.

# Notes

- I tested this with StraightTalk and Hollogram.io simcards. Both worked, but only the hollogram card would allow data if using the serial over USB connection. Both worked for texting.
- When sending a text to a number, you need either of these two formats: +19078675309 or 19078675309. Some phones will not accept unless you have the "+" sign in front of the number. So far, all phones accept the text when you use the "+" sign.
- The device has 2 batteries. One 9 volt battery, which is disposable, and non-chargable. The second battery is a 3.7 volt battery for the modem, which is charged by power from either board input or the 9 volt battery. The power battery percentage from the phone on the status bar is actually for the modem battery, and doesn't tell you how much battery power you have left in the 9 volt battery, unfortunately.
- The serial over USB works, and you can talk to the board that way, sending and receiving texts, etc.
- The included truth table outlines the different screens and button combinations.
- The bone phone was also supposed to have a mini browser/ftp/http get/put method, as well as a game, but I ran out of memory on the device. Come on, I only had 32 kb of space and 2 kb of ram to work with!

# About - Or, Why "bone phone"?

Well, the design is somewhat dog bone treat shaped, to protect the buttons. Add to that the fact that the original that I 3d printed was white, it did look like a big bone. While showing it to my co-workers one day, one of them said it's a bone. One then said it was a bone phone. They then all started laughing and we nicknamed it the bone phone.

I built this phone so I can learn about cell phones, c and c++ programming, and work with open source open hardware on a project. This bone phone met all those goals. This bone phone cannot make phone calls, but can send or receive texts.

# Hardware Components

The main board is an Arduino Uno (or knock off), and a Botletics LTE shield, both of which are open source hardware. There is also a Nokia 5110 screen, a slider (on/off) dpst switch, and 6 momentary switches, made by QTEATAK. There were also 6 550 ohm resistors, and a 3.7v lithium cell phone battery that I took out of an old HTC sense cell phone, but any 3.7volt LiPo/Li-ion battery would do.

I also included the files for the 3d printed case that I made. I used a glue gun and glue sticks to hold most of the components and wiring in place. Please see my website for tutorials about how it was put together.

The hardware used was specifically selected because it is all open source hardware.

"Open source hardware is hardware whose design is made publicly available so that anyone can study, modify, distribute, make, and sell the design or hardware based on that design."

# License

My portion of the software is licensed under the Apache2.0 license:

  Copyright 2019 by AlaskaLinuxUser (https://thealaskalinuxuser.wordpress.com)

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.


The software includes three libraries:

- The Adafruit Phona Library - Written by Limor Fried/Ladyada for Adafruit Industries. BSD license, all text above must be included in any redistribution. Which was modified by Botletics, maintaining the BSD license.
- The PCD8544 Library - The MIT License (MIT), Copyright (c) 2013 Carlos Rodrigues <cefrodrigues@gmail.com>
- The Arduino software serial library - GPL2.0

